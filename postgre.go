package postgre

import (
	"context"
	"errors"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jackc/tern/migrate"
)

type Postgre struct {
	Pool    *pgxpool.Pool
	Context context.Context
}

func (p *Postgre) connect(configLink string) error {
	p.Context = context.Background()

	var err error
	p.Pool, err = pgxpool.Connect(context.Background(), configLink)
	if err != nil {
		return errors.New("Unable to connection to database: " + err.Error())
	}

	return nil
}

func (p *Postgre) RunMigrate() (int, error) {
	conn, err := p.Pool.Acquire(context.Background())
	if err != nil {
		return 0, errors.New("Unable to acquire a database connection: " + err.Error())
	}
	defer conn.Release()

	migrator, err := migrate.NewMigrator(p.Context, conn.Conn(), "schema_version")
	if err != nil {
		return 0, errors.New("Unable to create a migrator: " + err.Error())
	}

	if err := migrator.LoadMigrations("./migrations"); err != nil {
		return 0, errors.New("Unable to load migrations: " + err.Error())
	}

	if err := migrator.Migrate(p.Context); err != nil {
		return 0, errors.New("Unable to migrate: " + err.Error())
	}

	ver, err := migrator.GetCurrentVersion(p.Context)
	if err != nil {
		return 0, errors.New("Unable to get current schema version: " + err.Error())
	}

	return int(ver), nil
}

func (p *Postgre) Execute(sql string, arguments ...interface{}) (pgconn.CommandTag, error) {
	result, err := p.Pool.Exec(p.Context, sql, arguments...)
	return result, err
}

func (p *Postgre) Query(sql string, arguments ...interface{}) (pgx.Rows, error) {
	return p.Pool.Query(p.Context, sql, arguments...)
}

func (p *Postgre) Close() {
	p.Pool.Close()
}
