module gitlab.com/medcloud-services/support/postgre

go 1.16

require (
	github.com/jackc/pgconn v1.10.0
	github.com/jackc/pgx/v4 v4.13.0
	github.com/jackc/tern v1.12.5
)
