package postgre

import (
	"errors"
)

type Config struct {
	Host string
	Port string
	Name string
	User string
	Pass string
}

func Init(cnf *Config) (*Postgre, error) {
	configLink, err := GetConfigLink(cnf)
	if err != nil {
		return nil, err
	}

	pgIns := Postgre{}
	if err := pgIns.connect(configLink); err != nil {
		return nil, err
	}

	return &pgIns, nil
}

func GetConfigLink(cnf *Config) (string, error) {
	if err := checkConfig(cnf.Host, cnf.Port, cnf.Name, cnf.User, cnf.Pass); err != nil {
		return "", err
	}

	return "postgres://" + cnf.User + ":" + cnf.Pass + "@" + cnf.Host + ":" + cnf.Port + "/" + cnf.Name, nil
}

func checkConfig(host, port, baseName, user, pass string) error {
	if host == "" {
		return errors.New("Cannot initialize connection to Postgre, host not set.  database.host")
	}

	if port == "" {
		return errors.New("Cannot initialize connection to Postgre, port not set.  database.port")
	}

	if baseName == "" {
		return errors.New("Cannot initialize connection to Postgre, base-name not set.  database.name")
	}

	if user == "" {
		return errors.New("Cannot initialize connection to Postgre, user not set.  database.user")
	}

	if pass == "" {
		return errors.New("Cannot initialize connection to Postgre, pass not set.  database.pass")
	}

	return nil
}
